const str = '5.5  + 5.5=';
function Calc(input) {
    if(input[input.length-1] == '=')
    {
		var result = 0,
			matchArr = [],
			searchPattern = /\-?\d+(\.\d+)?|[\+,\-,\*,\/,\=]{1}/ig;

		matchArr = input.match(searchPattern);

		if(matchArr[0]*1+"" !== "NaN") {
			result += matchArr[0]*1;
		}

		for(var i = 0; i < matchArr.length; i++) {
			switch(matchArr[i]) {
				case "+": result += matchArr[i+1] * 1; break;
				case "-": result -= matchArr[i+1] * 1; break;
				case "*": result *= matchArr[i+1] * 1; break;
				case "/": result /= matchArr[i+1] * 1; break;
				case "=": break;
				default: continue; break;
			}
		}
		return result.toFixed(2);
    }
        else
            {
                alert("= must be at the and of string")
            }
	}
 a = Calc(str)
 alert(a)