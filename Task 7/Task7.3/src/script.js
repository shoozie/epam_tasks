class Service{
    constructor() {
    this.collection = [];
  }
  
    idtypecheck(id){
        if(typeof id === "string" || id instanceof String)
          {
              return true
          }
        else
        {
           alert("id must be string")
           return false
        }
    }
  add(given_id, object){
      if(this.idtypecheck(given_id))
      {
      var acceptpush = true
      for(let obj in this.collection)
        {
            if(this.collection[obj][0] == given_id)
            {
                alert("Id must be unique")
                acceptpush = false
                break;
            }
         
        }
         if(acceptpush)
         {
          this.collection.push([given_id, object])   
         }
      }

  }
  GetAll(){
      return this.collection;
  }
  GetById(given_id){
      if(this.idtypecheck(given_id))
      {
         for(let obj in this.collection)
         {
             if(this.collection[obj][0] == given_id)
             {
                 return this.collection[obj][1];
             }
             
         }
         return null;
      }
    }
    DeleteById(given_id){
        if(this.idtypecheck(given_id))
      {
            for(let obj in this.collection)
         {
             if(this.collection[obj][0] == given_id)
             {
                this.collection.splice(obj,1);
             }
             
         }
      }
      
    }
    UpdateById(given_id, given_obj){
        if(this.idtypecheck(given_id))
      {
            for(let obj in this.collection)
         {
             if(this.collection[obj][0] == given_id)
             {
                this.collection[obj][1] = given_obj;
             }
             
         }
      }
    }
    ReplaceById(given_id, given_obj)
    {   
        let count = 0;
        for (let key in given_obj) {
            count++
        }
        if(this.idtypecheck(given_id) && Array.isArray(given_obj) && count == 2 && this.idtypecheck(given_obj[0]))
      {
            for(let obj in this.collection)
         {
             if(this.collection[obj][0] == given_id)
             {
                this.collection[obj] = given_obj;
             }
             
         }
      }
      else
      {
          alert('Id to replace not found')
      }
    }
}

storage = new Service();
storage.add('124',"ssss");
storage.add('123',"dlfhgldihf")
a = storage.GetAll();
storage.ReplaceById('124',['1234','fgjdfpjgpdoifjg','123123'])
alert(storage.GetAll());