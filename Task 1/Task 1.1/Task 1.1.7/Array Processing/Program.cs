﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Array_Processing
{
    public class Program
    {

        public static void MaxAndMin(int[] arr)
        {
            int max=arr[0];
            int min=arr[0];
            for(int i=0;i<arr.Length;i++)
            {
                if(max<arr[i])
                {
                    max=arr[i];
                }
                if(min>arr[i])
                {
                    min=arr[i];  
                }
            }
            Console.WriteLine("max value is:"+max);
            Console.WriteLine("min value is:"+min);
        }
       public static void ArrayOut(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"array[{i}]=" + arr[i]);
            }
        }

        static int[] BubbleSort(int[] mas)
        {
            int temp;
            for (int i = 0; i < mas.Length; i++)
            {
                for (int j = i + 1; j < mas.Length; j++)
                {
                    if (mas[i] > mas[j])
                    {
                        temp = mas[i];
                        mas[i] = mas[j];
                        mas[j] = temp;
                    }
                }
            }
            return mas;
        }
         static int[] Sort(int[] arr) //using bubble sorting
        {
            int temp;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("Initilize array capacity");
            try
            {
                int n = Int32.Parse(Console.ReadLine());
                int[] array = new int[n];
                Random random = new Random();
                Console.WriteLine("Randomly generated array:");
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = random.Next(-1000, 1000);
                    Console.WriteLine($"array[{i}]=" + array[i]);
                }
                MaxAndMin(array);
                Sort(array);
                Console.WriteLine("Sorted array:");
                ArrayOut(array);
            }
            catch(FormatException)
            {
                Console.WriteLine("Unable to parse input values");
            }
        }
    }
}