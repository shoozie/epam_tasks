﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Another_Triangle
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Insert integer N");
            try
            {
                int n = Int32.Parse(Console.ReadLine());
                for (int i = 1; i <= n; i++)
                {
                    Console.WriteLine(new String(' ', n-i)+new String('*', 2*i-1));
                }
            }
            catch(FormatException)
            {
                Console.WriteLine("Unable to parse input values");
            }

        }
    }
}