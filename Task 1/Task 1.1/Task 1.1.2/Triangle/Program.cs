﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Triangle // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Insert integer N");
            try
            {
                int n = Int32.Parse(Console.ReadLine());
                string str = String.Empty;
                for (int i = 1; i <= n; i++)
                {
                    str = new string('*', i);
                    Console.WriteLine(str);
                }
            }
            catch(FormatException)
            {
                Console.WriteLine("Unable to parse input");
            }
        }
    }
}