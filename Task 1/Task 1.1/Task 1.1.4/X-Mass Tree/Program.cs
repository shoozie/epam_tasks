﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace X_Mass_Tree
{
    public class Program
    {

        public static void Main(string[] args)
        {
            Console.WriteLine("Insert integer k"); // amount of triangles
            try
            {
                int n = Int32.Parse(Console.ReadLine());
                for (int c = 0; c < n; c++) //loop, that build each triangle in part of a tree
                {
                    for (int i = 1; i <= c+1; i++)
                    {

                        Console.WriteLine(new String(' ', n - i) + new String('*', 2 * i - 1));

                    }
                }
            }
            catch(FormatException)
            {
                Console.WriteLine("Unable to parse input values");
            }
        }
    }
}