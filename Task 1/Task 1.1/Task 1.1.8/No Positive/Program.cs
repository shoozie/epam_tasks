﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace No_Positie // Note: actual namespace depends on the project name.
{
    public class Program
    {

        public static void NoPositive(double[,,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    for (int k = 0; k < arr.GetLength(2); k++)
                    {
                        if(arr[i, j, k] >0)
                        {
                            arr[i, j, k] = 0;
                        }
                    }
                }
            }
        }
        public static void ArrayOut(double[,,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    for (int k = 0; k < arr.GetLength(2); k++)
                    {
                        Console.WriteLine($"array[{i},{j},{k}]=" + arr[i, j, k]);
                    }
                }
            }
        }
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Initilize i-demension");
                int n = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Initilize j-demension");
                int m = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Initilize k-demension");
                int l = Int32.Parse(Console.ReadLine());
                double[,,] array = new double[n, m, l];
                Random random = new Random();
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        for (int k = 0; k < l; k++)
                        {
                            array[i, j, k] = random.Next(-1000, 1000);
                        }
                    }
                }

                Console.WriteLine("Randomly generated array:");
                ArrayOut(array);
                NoPositive(array);
                Console.WriteLine("Modified array:");
                ArrayOut(array);
            }
            catch(FormatException)
            {
                Console.WriteLine("Unable to parse input values");
            }
        }
    }
}