﻿
using System.Collections.Generic;
using System.Linq;

namespace LowerCase
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Input string:");
            string str = Console.ReadLine();
            if (!string.IsNullOrEmpty(str))
            {
                string[] stringarray = str.Split(new char[] { '.', ',', '!', ':', ';', '?', '!', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                int counter = 0;
                for (int i = 0; i < stringarray.Length; i++)
                {
                    if (char.IsLower(stringarray[i][0]))
                    {
                        counter++;
                    }


                }
                Console.WriteLine("Words starting with lowercase:" +" "+counter);
            }
            else
            {
                Console.WriteLine("Input is NULL");
            }
        }
    }
}

