﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyApp // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int lettercount=0;
            String str = Console.ReadLine();
            if (!string.IsNullOrEmpty(str))
            {
                string[] splitarray = str.Split(new char[] { ' ', '.', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in splitarray)
                {
                    lettercount = s.Length + lettercount;
                }
                Console.WriteLine("Average amount of letters in word:" +" "+ lettercount / splitarray.Length);
            }
            else
            {
                Console.WriteLine("Input string is NULL");
            }
            
        }
    }
}
