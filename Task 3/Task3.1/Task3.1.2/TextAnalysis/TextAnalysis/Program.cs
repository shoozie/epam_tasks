﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Text;

namespace TextAnalysis 
{
    public class Program
    {

        public static string[] TextSeparator(string str)
        {
           string[] strarray = str.Split(new char[] { '.', ',', '!', ':', ';', '?', '!', ' ' }, StringSplitOptions.RemoveEmptyEntries);
           return strarray;

        }
        public static string[] ToLowerCase(string[] str)
        {
          return str.Select(str=>str.ToLower()).ToArray();
        }
        public static List<string> WordAdder(string[] strarray)
        {
            return strarray.ToList();
        }
        public static int WordAnalysis(List<string> words, string searchword)
        {
            return words.Count(word=>word==searchword);
        }
        public static Dictionary<string, int> AllWordsCounter(List<string> words)
        {
            return words.GroupBy(word=>word).ToDictionary(word=>word.Key, word=>word.Count());
        }
        public static Dictionary<string, int> OverallCount(Dictionary<string, int> dict)
        {
           return dict.Where(x=>x.Value>5).ToDictionary(x=>x.Key,x=>x.Value);
        }
        public static void Main(string[] args)
        {
            int x = 200;
            int y = 50;
            Console.SetWindowSize(x+1,y+1);
            Console.SetBufferSize(x + 1, y + 1);
            Console.WriteLine("Insert text:");
            Console.WriteLine();
            string str = Console.ReadLine();
            Console.WriteLine(new String('-',x));
            Console.WriteLine("Text Analysis");
            Console.WriteLine(new String('-', x));
            string[] strarray = TextSeparator(str);
            string[] strarray_tolower = ToLowerCase(strarray);
            List<string> words = WordAdder(strarray_tolower);
            Dictionary<string, int> uniquewords = AllWordsCounter(words);
            foreach (var word in uniquewords)
            {
                Console.WriteLine($"Word {word.Key} occures {word.Value} times");
            }
            Console.WriteLine(new String('-', x));
            Dictionary<string, int> overusedwords = OverallCount(uniquewords);
            if(overusedwords.Count > 0)
            {
                Console.WriteLine("Overused words");
                foreach (var word in overusedwords)
                {
                    Console.WriteLine($"Word {word.Key} occures {word.Value} times");
                }
            }
            else
            {
                Console.WriteLine("Text seems fine");
            }
            Console.WriteLine(new String('-', x));


        }
    }

}
