﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeakestLink // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Game(List<int> list)
        {
            Console.WriteLine("Введите, какой по счету человек будет вычеркнут каждый раунд:");
            int index = Int32.Parse(Console.ReadLine());
            if (index < list.Count && index>0)
            {
                while (list.Count > 0 && list.Count>=index)
                {
                    if (list.Count > index)
                    {
                        list.RemoveAt(index);
                        Console.WriteLine("Вычеркнут человек. Людей осталось:" + " " + list.Count);

                    }
                    else
                    {
                        list.RemoveAt(index - 1);
                        Console.WriteLine("Вычеркнут человек. Людей осталось:" + " " + list.Count);
                        Console.WriteLine("Игра окончена. Невозможно вычеркнуть больше людей");
                    }
                }
            }
            else
            {
                Console.WriteLine("Невозможно вычеркнуть данное число людей");
            }
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите число игроков.");
            string str = Console.ReadLine();
            int n = Int32.Parse(str);
            List<int> playerlist = new List<int>(Enumerable.Range(0,n));
            Game(playerlist);
            
        }
    }
}