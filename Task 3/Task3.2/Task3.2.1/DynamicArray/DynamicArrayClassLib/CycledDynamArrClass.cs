﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DynamicArrayClassLib
{
    public class CycledDynamicArray<T> :DynamicArray<T>, IEnumerable<T>
    {
        private int _loop;
        public CycledDynamicArray(int loop, DynamicArray<T> dynamicarray) :base(dynamicarray)
        {
            this._loop = loop;
        }
        public override IEnumerator<T> GetEnumerator()
        {
            for (int j = 0; j < _loop; j++)
            {
                for (int i = 0; i < array.Count(); i++)
                {
                    yield return array[i];
                }
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
