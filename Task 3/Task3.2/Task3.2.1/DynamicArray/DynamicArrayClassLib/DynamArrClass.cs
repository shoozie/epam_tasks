﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
namespace DynamicArrayClassLib
{
    public class DynamicArray<T> : IEnumerable<T>, ICloneable
    {
        public int Capacity { get; private set; }
        public int Count { get; private set; }
        protected T[] array;

        public DynamicArray()
        {
            Capacity = 8;
            array = new T[Capacity];
        }
        public DynamicArray(int capacity)
        {
            this.Capacity = capacity;
            array = new T[capacity];
        }
        public DynamicArray(IEnumerable<T> collection)
        {
            Capacity = collection.Count();
            array = new T[Capacity];
            foreach (var item in collection)
            {
                Add(item);
            }
        }
        public void Add(T item)
        {
           
            if (Count == Capacity)
            {
                SetCapacity(Capacity * 2);
                array[Count] = item;
                Count++;
            }
            else
            {
                array[Count] = item;
                Count++;
            }

        }
        public bool Contains(T item)
        {
            foreach (T item2 in array)
            {
                if (item2.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
        public int IndexOf(T item)
        {
                int index =-1;
                for (int i = 0; i < Count; i++)
                {
                    if (array[i].Equals(item))
                    {
                        index = i;
                    }
                }
                return index;
        }
        public void AddRange(IEnumerable<T> collection)
        {
            int newcount = Count+ collection.Count();
            if (Count + collection.Count() > Capacity)
            {
                SetCapacity(Capacity + collection.Count());
                for (int i = 0; i < collection.Count(); i++)
                {
                    Add(collection.ElementAt(i));
                }
            }
            else
            {
                foreach(var item in collection)
                {
                    Add(item);
                }
            }
        }
        public bool Insert(T item, int index)
        {
            if (index <= Count)
            {
                Count++;
                if (Count >= Capacity)
                {
                    Capacity *= 2;
                    T[] temp = new T[Capacity];
                    array.CopyTo(temp, 0);
                    array = new T[Capacity];
                    for (int i = 0; i <= index; i++)
                    {
                        if (i == index)
                        {
                            array[i] = item;
                        }
                    }
                    for (int i = index; i < Count; i++)
                    {
                        array[i + 1] = temp[i];

                    }
                    return true;
                }
                else
                {
                    T[] temp = new T[Capacity];
                    array.CopyTo(temp, 0);
                    array = new T[Capacity];
                    for (int i = 0; i <= index; i++)
                    {
                        array[i] = temp[i];
                        if (i == index)
                        {
                            array[i] = item;
                        }
                    }
                    for (int i = index; i < Count; i++)
                    {

                        array[i + 1] = temp[i];

                    }
                    return true;
                }
            }
            else
            {
                return false;
            }

        }
        public bool Remove(T item)
        {
            if (Contains(item))
            {
                Count--;
                for (int i = IndexOf(item); i < Count; i++)
                {
                    array[i] = array[i + 1];
                }

                return true;
            }
            else
                return false;
        }
        public void SetCapacity(int value)
        {
            if (value <= Count)
            {
                T[] temp = new T[Capacity];
                array.CopyTo(temp, 0);
                Count = value;
                Capacity = value;
                array = new T[Capacity];
                for (int i = 0; i < Count; i++)
                {
                    array[i] = temp[i];
                }
            }
            else
            {
                T[] temp = new T[Capacity];
                array.CopyTo(temp, 0);
                Capacity = value;
                array = new T[Capacity];
                for (int i = 0; i < Count; i++)
                {
                    array[i] = temp[i];
                }
            }


        }

        public T this[int index]
        {
            get
            {
                if (index > (-1) * Count && index < 0)
                {
                    return array[index - index - index];
                }
                else if (index < Count)
                {
                    return array[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set { array[index] = value; }
        }
        public virtual IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return array[i];
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public object Clone() => new DynamicArray<T>(this);
        public T[] ToArray()
        {
            T[] usualarray = new T[Count];
            for (int i = 0; i < Count; i++)
            {
                usualarray[i] = array[i];
            }
            return usualarray;
        }

    }
}