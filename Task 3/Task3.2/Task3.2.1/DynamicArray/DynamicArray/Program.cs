﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using DynamicArrayClassLib;
namespace Task3_2_1 // Note: actual namespace depends on the project name.
{
    public class Program 
    {
        public static void Main(string[] args)
        {
            DynamicArray<int> d = new DynamicArray<int>(); //some test case for AddRange
            for (int i = 0; i < 10; i++)
            {
                d.Add(i);
            }
            CycledDynamicArray<int> cd = new CycledDynamicArray<int>(5, d);
            foreach(var item in cd)
            {
                Console.WriteLine(item);
            }
        }
    }
}