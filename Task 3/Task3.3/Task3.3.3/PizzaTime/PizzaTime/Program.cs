﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PizzaTime 
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Pizza p1 = new Pizza("Pepperoni", 10, 350);
            Pizza p2 = new Pizza("Margherita", 7, 250);
            Pizza p3 = new Pizza("Special", 5, 300);
            try
            {
                TakеOutScreen tk = new TakеOutScreen();
                while (true)
                {
                    Console.WriteLine("1. Создать заказ");
                    int input = Int32.Parse(Console.ReadLine());
                    switch (input)
                    {
                        case 1:
                            {
                                Customer customer = new Customer(new Order());
                                customer.CustomerOrder.OnOrderReady += tk.PrintReadyOrders;
                                customer.CustomerOrder.OnOrderCreated += customer.WriteName;
                                tk.OnDisplayed += customer.TakeOrder;
                                while (input != 4)
                                {
                                    Console.WriteLine("Выберте пиццу:");
                                    Console.WriteLine($"1.Pepperoni цена {p1.Price}");
                                    Console.WriteLine($"2.Margherita цена {p2.Price}");
                                    Console.WriteLine($"3.Special цена {p3.Price}");
                                    Console.WriteLine($"4.Закончить заказ");
                                    input = Int32.Parse(Console.ReadLine());
                                    switch (input)
                                    {
                                        case 1:
                                            {
                                                Console.WriteLine("Кол-во:");
                                                int count = Int32.Parse(Console.ReadLine());
                                                customer.AddPizza(p1, count);
                                            }
                                            break;
                                        case 2:
                                            {
                                                Console.WriteLine("Кол-во:");
                                                int count = Int32.Parse(Console.ReadLine());
                                                customer.AddPizza(p2, count);

                                            }
                                            break;
                                        case 3:
                                            {
                                                Console.WriteLine("Кол-во:");
                                                int count = Int32.Parse(Console.ReadLine());
                                                customer.AddPizza(p3, count);
                                            }
                                            break;
                                        case 4:
                                            {
                                                Console.WriteLine("Введите имя:");
                                                string name = Console.ReadLine();
                                                customer.Name = name;
                                                tk.AddOrder(customer.CustomerOrder);
                                                customer.CustomerOrder.PrintCheck();
                                                customer.CustomerOrder.Ready();
                                            }
                                            break;
                                    }

                                }
                            }
                            break;
                    }

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Не удалось преобразовать input");
            }
        }
    }
}