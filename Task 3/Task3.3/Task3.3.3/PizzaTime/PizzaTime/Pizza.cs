﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaTime
{
    public class Pizza
    {
        public string Name { get; private set; }
        public int CoockingTime { get; private set; }
        public double Price { get; private set; }
        public Pizza(string name, int time, int price)
        {
            Name = name;
            CoockingTime = time;
            Price = price;
        }


    }
}
