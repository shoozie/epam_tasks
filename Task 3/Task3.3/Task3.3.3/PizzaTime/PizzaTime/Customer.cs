﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaTime
{
    public class Customer
    {
        public string Name { get; set; }
        public int OrderNumber { get; set; }
        public Order CustomerOrder = new Order();
        public Customer(Order o)
        {
            this.CustomerOrder = o;
            this.OrderNumber = o.OrderId;
        }
        public void AddPizza(Pizza pizza, int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                CustomerOrder.pizzalist.Add(pizza);
            }
        }
        public void TakeOrder()
        {
            Console.WriteLine($"Заказ {CustomerOrder.OrderId} забрали");
        }
        public void WriteName()
        {
            Console.WriteLine(Name + ",");
        }

    }
}
