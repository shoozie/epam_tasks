﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaTime
{
    public class TakеOutScreen
    {
        public event Action OnDisplayed = delegate { };
        public List<Order> orders = new List<Order>();
        public void PrintReadyOrders()
        {
            foreach (var item in orders)
            {
                if (item.OrderStatus == "Ready")
                {
                    Console.WriteLine($"ваш заказ с уникальным номером {item.OrderId} готов");
                }

            }
            OnDisplayed();
            RemoveReadyOrders();
        }
        public void AddOrder(Order o)
        {

            orders.Add(o);
        }
        public void RemoveReadyOrders()
        {
            var orderstoremove = orders.Where(o => o.OrderStatus == "Ready").ToList();
            orders.RemoveAll(x=>orderstoremove.Contains(x));
        }
    }
}
