﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaTime
{

    public class Order
    {
        public event Action OnOrderReady;
        public event Action OnOrderCreated;
        public List<Pizza> pizzalist = new List<Pizza>();
        public string OrderStatus { get; set; }
        public int OrderId
        {
            get { return this.pizzalist.GetHashCode();}
            private set {;}
        }
        public double GetOverallPrice()
        {
            return pizzalist.Sum(p => p.Price);
        }
        public int GetTimeToCoock()
        {
            var maxtimepizza = pizzalist.OrderByDescending(x => x.CoockingTime).First();
            int maxtime = maxtimepizza.CoockingTime;
            return maxtime;
        }
        public void Ready()
        {
            OrderStatus = "Ready";
            OnOrderReady();
        }
        public void PrintCheck()
        {
            Console.WriteLine($"Заказ на сумму:{GetOverallPrice()} будет готов через: {GetTimeToCoock()} минут");
            OnOrderCreated();
        }
        public Order()
        {
            this.OrderStatus = "Not ready";
        }
    }
}
