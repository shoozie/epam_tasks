﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SuperString
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string str = "1234012341234543";
            Console.WriteLine(str.LanguageCheck());
        }
    }
    public static class StringExtension
    {
        public static string RemoveAllPunct(this string str)
        {
            string[] temparray = str.Split(new char[] { '!', '?', '.', ',', ' ', ':', ';' }, StringSplitOptions.RemoveEmptyEntries);
            string temp = string.Join("", temparray);
            return temp;
        }
        public static string LanguageCheck(this string str)
        {
            string stringcheck = "";
            if (!String.IsNullOrEmpty(str))
            {
                HashSet<char> rulower = Enumerable.Range(0, 32).Select((x, i) => (char)('а' + i)).ToHashSet();
                HashSet<char> ruupper = Enumerable.Range(0, 32).Select((x, i) => (char)('А' + i)).ToHashSet();
                HashSet<char> allru = rulower.Concat(ruupper).ToHashSet();
                allru.Add((char)1025);                                                                        //ё
                allru.Add((char)1105);                                                                        //Ё
                HashSet<char> enlower = Enumerable.Range(0, 26).Select((x, i) => (char)('a' + i)).ToHashSet();
                HashSet<char> enupper = Enumerable.Range(0, 26).Select((x, i) => (char)('A' + i)).ToHashSet();
                HashSet<char> allen = enlower.Concat(enupper).ToHashSet();
                string temp = str.RemoveAllPunct();
                if (temp.All(x => allru.Contains(x)))
                {
                    stringcheck = "Russian";
                }
                else if (temp.All(x => allen.Contains(x)))
                {
                    stringcheck = "English";
                }
                else if (temp.All(x => char.IsDigit(x)))
                {
                    stringcheck = "Number";
                }
                else
                {
                    stringcheck = "Mixed";
                }
                return stringcheck;
            }
           else
            {
               stringcheck = "Input is empty";
                return stringcheck;
            }
        }

    }
}
