﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SuperArray 
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double[] arr = { 1, 2, 3, 5, 7};
            arr.ActionToElement((double element)=>element*element); // example of action (math.pow)
            arr.ArrayPrinter();
            Console.WriteLine("----------");
            Console.WriteLine(arr.MostCommonItem());
        }
    }
    public static class ArrayExtension
    {
        public static void ActionToElement(this double[] arr, Func<double, double> action)
        {
            for(int i=0; i<arr.Length; i++)
            {
                arr[i] = action(arr[i]);
            }
        }
        public static double ExtArraySum(this double[] arr)
        {
            return arr.Sum();
        }
        public static double ExtArrayAverage(this double[] arr)
        {
            return arr.Average();
        }
        public static double MostCommonItem(this double[] arr)
        {
            var mostcommon = arr.GroupBy(x => x).OrderByDescending(x => x.Count()).FirstOrDefault(); //returns first element if all elements are equally common.
            return mostcommon.Key;
        }

        public static void ArrayPrinter(this double[] arr)
        {
            foreach(var element in arr)
            {
                Console.WriteLine(element);
            }
        }
    }

}