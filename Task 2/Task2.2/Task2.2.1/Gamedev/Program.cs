﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gamedev.Class;




namespace Gamedev 
{

   // goal is chase every enemy '#', every time you step on it, combat engages.While your health is positive, you roll a Dices, so do enemy.
   //Every roll determines, who will land a hit.
   //You have heath, damage and speed. Speed make's easier the roll.
    public class Program
    {
        static readonly int x = 150;
        static readonly int y = 60;
        public static void Main(string[] args)
        {
            Console.SetWindowSize(x + 1, y + 1);
            Console.SetBufferSize(x + 1, y + 1);
            MainCharacter mc = new MainCharacter(1, 50, 20, '@');
            Point p = new Point(6,6);
            Point p1 = new Point(10,10);
            Point p2 = new Point(20,20);
            Point p3 = new Point(34, 5);
            Point p4 = new Point(3, 20);
            Point garbage = new Point(2,50);
            Enemy e = new Enemy(1, 70, 10, '#');
            Enemy e1 = new Enemy(1, 70, 10, '#');
            Enemy e2 = new Enemy(1, 70, 10, '#');
            Enemy e3 = new Enemy(1, 70, 5, '#');
            mc._location=p;
            e._location=p1;
            e1._location=p2;
            e3._location=p3;
            e2._location=p4;
            Wall w1 = new Wall('-');
            Wall w2 = new Wall('|');
            Wall w3 = w1;
            Wall w4 = w2;
            Wall w5 = w1;
            Wall w6 = w2;
            Wall w7 = w1;
            Wall w8 = w2;
            List<Wall> walls = new List<Wall>() { w1, w2 ,w3,w4,w5,w6,w7,w8};
            w1.HorizontallWall(50,0,0);
            w2.VerticalWall(25,0,0);
            w4.VerticalWall(25, 50, 0);
            w3.HorizontallWall(50, 0, 25);
            w6.VerticalWall(8, 25, 0);
            w5.HorizontallWall(8, 25, 7);
            w1.HorizontallWall(10, 0, 10);
            w2.VerticalWall(5, 10, 10);
            w8.VerticalWall(10, 30, 15);
            w7.HorizontallWall(4, 30, 15);
            HealingSalve hs = new HealingSalve('h',7,7,50);
            HealingSalve hs1 = new HealingSalve('h',40, 15, 100);
            WeaponSharper ws = new WeaponSharper('w', 40, 4, 20);
            SpeedBooster sb = new SpeedBooster('s', 4, 20, 1);
            List<Enemy> enemies = new List<Enemy>() { e, e1, e2 ,e3 };
            List<Bonus> bonus = new List<Bonus>() { hs, hs1,ws,sb };
            foreach(Bonus buff in bonus)
            {
                buff.GetLocation().DrawPoint(buff.GetSymbol());
            }
                while (mc.GetHealth() > 0)
                {

                    int a = 52;
                    int b = 1;
                    Console.SetCursorPosition(a, b);
                    Console.WriteLine("Battle Log:");
                    Console.SetCursorPosition(a, b + 1);
                    Console.WriteLine("HP:" + " " + mc.GetHealth());
                    Console.SetCursorPosition(a, b + 2);
                    Console.WriteLine("Speed:" + " " + mc.GetSpeed());
                    Console.SetCursorPosition(a, b + 3);
                    Console.WriteLine("Damage:" + " " + mc.GetDamageValue());
                    ConsoleKeyInfo consoleKeyInfo = Console.ReadKey();
                    mc.Move(consoleKeyInfo);
                    int logcounter = 0;
                    foreach (Enemy enemy in enemies)
                    {

                        if (enemy.IsDead() == false)
                        {
                            foreach (Bonus buff in bonus)
                            {
                                if (mc._location == buff._location || enemy._location == buff._location)
                                {
                                    buff._location = garbage;
                                    if (buff.GetType() == typeof(HealingSalve))
                                    {

                                        mc.SetHealth(buff.Effect());
                                    }
                                    else if (buff.GetType() == typeof(SpeedBooster))
                                    {
                                        mc.SetSpeed(buff.Effect());
                                    }
                                    else if (buff.GetType() == typeof(WeaponSharper))
                                    {
                                        mc.SetDamage(buff.Effect());
                                    }
                                }
                            }
                            enemy._location.Clear();
                            enemy.Move(enemy._location.x, enemy._location.y);
                            foreach (Wall wall in walls)
                            {
                                if (wall.WillBeHit(mc._location))
                                {
                                    mc._location.DrawPoint(wall.GetWallChar());
                                    Console.SetCursorPosition(mc._prevpoint.x, mc._prevpoint.y);
                                    mc._location = mc._prevpoint;
                                    mc._location.DrawPoint('@');
                                }
                                else if (wall.WillBeHit(enemy._location))
                                {
                                    enemy._location.DrawPoint(wall.GetWallChar());
                                    enemy._location = enemy._prevpoint;
                                    enemy._location.DrawPoint('#');
                                }
                            }
                            if (mc._location == enemy._location || mc._location == enemy._prevpoint)
                            {

                                int mchitcounter = 0;
                                int ehitcounter = 0;
                                while (enemy.GetHealth() > 0)
                                {

                                    Dices mcdice = new Dices();
                                    Dices edice = new Dices();
                                    int mcroll = mcdice.Roll();
                                    int eroll = edice.Roll();
                                    if (mcroll + mc.GetSpeed() > eroll + enemy.GetSpeed())
                                    {
                                        mchitcounter++;
                                        enemy.GetDamage(mc.GetDamageValue());
                                    }
                                    else if (eroll + enemy.GetSpeed() > mcroll + mc.GetSpeed())
                                    {
                                        ehitcounter++;
                                        mc.GetDamage(enemy.GetDamageValue());
                                    }
                                    logcounter++;
                                }
                                if (mc.GetHealth() > 0)
                                {

                                    Console.SetCursorPosition(a, b + 4 + logcounter);
                                    Console.WriteLine($"You won and hit the enemy with {mc.GetDamageValue()} damage {mchitcounter} times, enemy hits you {ehitcounter} times with {enemy.GetDamageValue()} damage");
                                }
                                else
                                {
                                    Console.SetCursorPosition(a, b + 4 + logcounter);
                                    Console.WriteLine($"You died and hit the enemy with {mc.GetDamageValue()} damage {mchitcounter} times, enemy hits you {ehitcounter} times with {enemy.GetDamageValue()} damage");
                                }

                            }
                        }
                }

            }
            Console.SetCursorPosition(0, 28);
            Console.WriteLine("GAME OVER");
            Console.Read();

        }
    }
}