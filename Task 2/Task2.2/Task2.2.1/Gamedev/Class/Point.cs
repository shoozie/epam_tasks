﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    struct Point
    {
        public int x { get; set; }
        public int y { get; set; }
        public static bool operator ==(Point a, Point b)
        {
            if (a.x == b.x && a.y == b.y)
            {
                return true;
            }
            else
                return false;
        }
        public static bool operator !=(Point a, Point b)
        {
            if(a==b)
            {
                return false;
            }
            else 
                return true;
        }
       public void DrawPoint(char ch)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(ch);
        }
        public void Clear()
        {
            Console.SetCursorPosition(x, y);
            Console.Write(' ');
        }
        public Point(int x1, int y1)
        {
            this.x = x1;
            this.y = y1;
        }

    }

}
