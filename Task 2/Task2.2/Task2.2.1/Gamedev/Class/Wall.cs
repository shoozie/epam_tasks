﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class Wall
    {
        private List<Point> _points = new List<Point>();
        private char _wallchar;
        public char GetWallChar() => _wallchar;
        public Wall(char c)
        {
            this._wallchar = c;

        }
        public void HorizontallWall(int length, int x, int y)
        {
            Point startpoint = new Point();
            startpoint.x = x;
            startpoint.y = y;
            startpoint.DrawPoint(_wallchar);
            _points.Add(startpoint);
            for (int i = 0; i < length; i++)
            {

                startpoint.x += 1;
                startpoint.DrawPoint(_wallchar);
                _points.Add(startpoint);
            }
        }
        public void VerticalWall(int lenght, int x, int y)
        {
            Point startpoint = new Point();
            startpoint.x = x;
            startpoint.y = y;
            startpoint.DrawPoint(_wallchar);
            _points.Add(startpoint);
            for (int i = 0; i < lenght; i++)
            {
                startpoint.y += 1;
                startpoint.DrawPoint(_wallchar);
                _points.Add(startpoint);
            }
        }
        public bool WillBeHit(Point p)
        {
            foreach (Point p1 in _points)
            {
                if (p1 == p)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
