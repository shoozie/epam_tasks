﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class WeaponSharper : Bonus
    {
        private int _damagebonus;
        public override int Effect() => _damagebonus;
        public WeaponSharper(char c, int x, int y, int d) : base(c, x, y)
        {
            _damagebonus = d;
        }
    }
}
