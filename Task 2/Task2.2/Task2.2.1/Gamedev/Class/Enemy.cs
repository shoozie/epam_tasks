﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class Enemy : Character, IDamagable, IMovable
    {

        public Point _prevpoint;
        public Enemy(int s, int h, int d, char ch) : base(s, h, d, ch)
        {

        }
        public void Move(int x, int y)
        {
            _prevpoint = _location;
            Random r = new Random();
            int v = r.Next(4);
            int direction = v;
            switch (direction)
            {
                case 0:
                    {
                        _location.Clear();
                        _location.x += 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
                case 1:
                    {
                        _location.Clear();
                        _location.x -= 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
                case 2:
                    {
                        _location.Clear();
                        _location.y += 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
                case 3:
                    {
                        _location.Clear();
                        _location.y -= 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
            }

        }
        public bool IsDead()
        {
            if (GetHealth() <= 0)
            {
                return true;
            }
            else
                return false;
        }

    }
}
