﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    struct Dices
    {
        private int dice1;
        private int dice2;
        public int Roll()
        {
            Random random = new Random();
            dice1 = random.Next(6) + 1;
            dice2 = random.Next(6) + 1;
            int roll = dice1 + dice2;
            return roll;
        }

    }
}
