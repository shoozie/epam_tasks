﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class Character : IDamagable
    {
        private char symbol;
        public char GetSymbol() => symbol;
        private int _speed;
        public int GetSpeed() => _speed;
        public void SetSpeed(int value)
        {
            _speed+= value;
        }
        public Point _location;
        private int _health;
        public int GetHealth() => _health;
        public void SetHealth(int value)
        {
            _health += value;
        }
        private int _damage;
        public int GetDamageValue() => _damage; 
        public void GetDamage(int damage)
        {
            this._health -= damage;
        }
        public void SetDamage(int value)
        {
            _damage+=value;
        }
        public Character(int s, int h, int d, char ch)
        {
            this._damage = d;
            this._health = h;
            this._speed = s;
            this.symbol = ch;
        }
        public virtual bool ISDead()
        {
            if(_health<=0)
            {
                return true;
            }
            else 
                return false;
        }
      
    }
   
}
