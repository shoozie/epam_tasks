﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class MainCharacter : Character, IDamagable, IMovable
    {
        public Point _prevpoint;
        public MainCharacter(int s, int h, int d, char ch) : base(s, h, d, ch)
        {

        }
        public void Move(ConsoleKeyInfo key)
        {

            _prevpoint = _location;
            _location.DrawPoint(GetSymbol());
            switch (key.Key)
            {

                case ConsoleKey.LeftArrow:
                    {
                        _location.Clear();
                        _location.x -= 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
                case ConsoleKey.RightArrow:
                    {
                        _location.Clear();
                        _location.x += 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
                case ConsoleKey.UpArrow:
                    {
                        _location.Clear();
                        _location.y -= 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
                case ConsoleKey.DownArrow:
                    {
                        _location.Clear();
                        _location.y += 1;
                        _location.DrawPoint(GetSymbol());
                    }
                    break;
            }



        }

    }
}
