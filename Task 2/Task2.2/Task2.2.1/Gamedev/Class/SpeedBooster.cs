﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class SpeedBooster : Bonus
    {
        private int _speedboost;
        public override int Effect() => _speedboost;
        public SpeedBooster(char c, int x, int y, int s) : base(c, x, y)
        {
            this._speedboost = s;
        }
    }
}
