﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class HealingSalve : Bonus
    {
        private int _power;
        public override int Effect() => _power;
        public HealingSalve(char c, int x, int y, int p) : base(c, x, y)
        {
            this._power = p;
        }
    }
}
