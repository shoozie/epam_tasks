﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamedev.Class
{
    class Bonus
    {
        private char _ch;
        public char GetSymbol() => _ch;
        public Point _location;
        public Point GetLocation() => _location;
        public void SetLocation(Point p)
        {
            _location = p;
        }
        public Bonus(char c, int x, int y)
        {
            this._ch = c;
            this._location.x = x;
            this._location.y = y;
        }
        public virtual int Effect()
        {
            return 0;
        }
    }
}
