﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyString;
namespace Task2_1_1
{ 
    public class Program
    {
        public static void Main(string[] args)
        {
            CustomString str = new CustomString();  // this is a Test file that shows CustomString features
            char[] arr = new char[] { 'a', 'b', 'c' };
            CustomString.ToCustomString(arr);
            CustomString str1 = new CustomString(new[] { 'e', 'f' });
            CustomString str2 = new CustomString(10); // Creating empty char array, that can contain 10 elements
            string s = "1234567890";
            str2 = CustomString.ToCustomString(s);
            CustomString str3 = str1 + str2;
            Console.WriteLine(str3);
            Console.WriteLine("_______");
            foreach(char c in str3)
            {
                Console.WriteLine(c);
            }
            Console.WriteLine("_______");
            Console.WriteLine(str3.IndexOf('1'));
            Console.WriteLine("_______");
            Console.WriteLine(str1==str3);
            Console.WriteLine("_______");
            Console.WriteLine(str1!=str3);
            str1.ToCharArray();
            foreach(char c in str1)
            {
                Console.WriteLine(c);
            }
        }
    }
}