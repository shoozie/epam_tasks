﻿namespace MyString
{
    using System.Collections;
 public  class CustomString : IEnumerable
    {
        private char[] chararray;

        private int lenght
        {
            get { return chararray.Length; }
        }
        public CustomString(char[] arr)
        {
            this.chararray = arr;
        }
        public CustomString()
        {
            chararray = new char[0];
        }
        public CustomString(int lenght)
        {
            chararray = new char[lenght];
        }


        public char this[int index]
        {
            get { return chararray[index]; }
            set { chararray[index] = value; }
        }
        public static CustomString operator +(CustomString str1, CustomString str2)
        {
            int sumlenght = str1.lenght + str2.lenght;
            CustomString str3 = new CustomString(sumlenght);
            for (int i = 0; i < str1.lenght; i++)
            {
                str3[i] = str1[i];
            }
            for (int i = 0; i < str2.lenght; i++)
            {
                str3[str1.lenght + i] = str2[i];
            }

            return str3;
        }
        public static bool operator ==(CustomString str1, CustomString str2)
        {
            if (str1.lenght != str2.lenght)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < str1.lenght; i++)
                {
                    if (str1[i] != str2[i])
                    {
                        return false;
                    }

                }
                return true;
            }

        }
        public static bool operator !=(CustomString str1, CustomString str2)
        {
            return !(str1 == str2);

        }

        public char[] ToCharArray()
        {
            char[] arr = new char[chararray.Length];
            for (int i = 0; i < chararray.Length; i++)
            {
                arr[i] = chararray[i];
            }
            return arr;
        }
        public static CustomString ToCustomString(char[] arr)
        {
            CustomString str1 = new CustomString(arr.Length);
            for (int i = 0; i < arr.Length; i++)
            {
                str1[i] = arr[i];
            }
            return str1;
        }
        public static CustomString ToCustomString(string str)
        {
            CustomString str1 = new CustomString(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                str1[i] = str[i];
            }
            return str1;
        }
        public override string ToString()
        {
            string str = new string(chararray);
            return str;
        }
        public override bool Equals(object? obj)
        {
            return Equals(obj as CustomString);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public IEnumerator GetEnumerator()
        {
            return chararray.GetEnumerator();
        }
        public int IndexOf(char c)
        {
            for (int i = 0; i < chararray.Length; i++)
            {
                if (chararray[i] == c)
                {
                    return i;
                }
            }
            return -1;
        }
        public CustomString Replace(char c, char replace)
        {
            CustomString str = new CustomString(chararray.Length);
            for (int i = 0; i < chararray.Length; i++)
            {

                if (chararray[i] == c)
                {
                    chararray[i] = replace;
                }
                else
                    str[i] = chararray[i];
            }
            return str;

        }
    }
}