﻿using System.Collections;
using System.Reflection;
namespace CustomPaint
{

    struct Point
    {
        public double X;
        public double Y;
        public Point(double x, double y)
        {
            X = x;
            Y = y;

        }
    }
    class Shapes
    {
        protected Point _center;
        public virtual double GetVolume() => 0;
        public virtual double GetLenght() => 0;
        public virtual string GetInfo()
        {
            string str = new string("Center coordinates:"+GetPoint(_center).ToString());
            return str;
        }
        public virtual (double,double) GetPoint(Point p)
        {
            return (p.X, p.Y);
        }
        public Shapes(Point p)
        {
            _center.X=p.X;
            _center.Y=p.Y;
        }
    }
    class RoundShape: Shapes
    {
        private double _radious;
        public double GetRadious() => _radious;
        public override string GetInfo()
        {
            return base.GetInfo()+" "+"Lenght:"+ GetLenght().ToString();
        }
        public override double GetLenght() => 2 * Math.PI * GetRadious();
        public double SetRadious(double value)
        {
            _radious = value;
            return _radious;
        }

        public RoundShape(Point p, double r) : base(p)
        {
            this._radious = r;

        }
    }
    class Circle : RoundShape
    {
        public override double GetVolume() =>  Math.PI * GetRadious()*GetRadious();
        public override string GetInfo()
        {
            return base.GetInfo()+" "+"Volume:"+GetVolume();
        }
        public Circle(Point p, double r) : base(p,r)
        {
           
        }
       
    }
    class Ring: Circle
    {
        private Circle _inner;
        private Circle _outer;
        public override double GetVolume()=> _outer.GetVolume() - _inner.GetVolume();
        public override double GetLenght()=> _outer.GetLenght()+_inner.GetLenght();
        public Ring(Point p, double Ir, double Or) : base (p,Or)
        {
            _inner = new Circle(p, Ir);
            _outer = new Circle(p, Or);
        }

    }
    class Vector : Shapes
    {
        private double _x; // Vector coordinates
        private double _y;
        private Point _endpoint; // Vector endPoint 
        public Point EndingPoint() => _endpoint;
        public override string GetInfo()
        {
            string str = new string("First point:" + GetPoint(_center).ToString() + " " + "Second Point:" +" "+ GetPoint(_endpoint).ToString());
            return str;
        }
        public Vector (Point p, Point p1): base(p) // Representing vector as difference between starting point and ending point 
        {
            this._endpoint = p1;
            this._x = p1.X - p.X;
            this._y = p1.Y - p.Y;
        }
        public override double GetLenght()=> Math.Sqrt(_x*_x+_y*_y); // Vector Lenght
        public Vector GetSum(Vector v1, Vector v2)
        {
            Vector v3 = new Vector(v1._center, v2._endpoint);
            return v3;
        }
        public double Multiply(Vector v1, Vector v2)
        {
            double result = v1._x*v2._y-v2._x*v1._y; // result of multiplying to vectors (Calculated as det of 2x2 Matrix )
            return result;
        }

    }
    class Squre : Vector 
    {
        protected Vector v; //Vector of Diagonal allows to represent a shape in a single way
        public Vector GetVector() => v;
        public override double GetLenght() => 2*Math.Sqrt(2)*v.GetLenght(); 
        public override double GetVolume() => 0.5*(v.GetLenght() * v.GetLenght()); 
        public Squre(Point p, Point p1) : base(p,p1)
        {
            this.v = new Vector(p, p1);
        }

    }
    class Triangle : Squre
    {
        protected Vector v1; //Representong a triangle as a 2 vectors and their sum
        public override double GetLenght() => v.GetLenght() + v1.GetLenght() + GetSum(v, v1).GetLenght();
        public override double GetVolume() => Math.Abs(0.5 * (Multiply(v, v1)));
        public override string GetInfo()
        {
            return base.GetInfo()+" "+"Third point:"+" "+GetPoint(v1.EndingPoint()).ToString();
        }
        public Triangle(Point p, Point p1, Point p2): base(p,p1)
        {
            this.v1 = new Vector(p1, p2);
        }
    }
    class Parallelogram : Triangle  // To set this figure 2 vectors will be enough
    {
        public override double GetLenght()=> 2*v.GetLenght()+2*v1.GetLenght();
        public override double GetVolume() => Math.Abs((Multiply(v, v1)));
        public Parallelogram(Point p, Point p1, Point p2) : base(p, p1,p2)
        {

        }
    }
    class Box : IEnumerable
    {
        private List<Shapes> shapes = new List<Shapes>();
        public void AddShape(Shapes s)
        {
            shapes.Add(s);
        }
        public void RemoveAll()
        {
            shapes.Clear();
        }
        public void ShowAll()
        {
            foreach (Shapes s in shapes)
            {
                Console.WriteLine(s.GetType().Name+" "+ s.GetInfo());
            }
        }
        public IEnumerator GetEnumerator()
        {
            return shapes.GetEnumerator();
        }
       
    }
    class User
    {
        private string _name;
        public string GetName()=> _name;
        public void SetName(string s)
        {
            _name = s;
        }
        public User(string name)
        {
            this._name = name;
        }
    }

    public class Program
    {
      
        public static void Main(string[] args)
        {

            Box box = new Box();
            Operation op = new Operation();
            Figures f = new Figures();
            Console.WriteLine("Insert user name");
            string str = Console.ReadLine();
            User user = new User(str);
            try
            {
                while (op!=Operation.Exit)
                {
                    
                    Console.WriteLine($"{user.GetName()}, please pick action:");
                    Console.WriteLine("Input:");
                    Console.WriteLine("\t 1: Add figure");
                    Console.WriteLine("\t 2: Show all");
                    Console.WriteLine("\t 3: Delete all");
                    Console.WriteLine("\t 4: Change user");
                    Console.WriteLine("\t 5: Exit");
                    int input = Int32.Parse(Console.ReadLine());
                    op = (Operation)input;
                    switch (op)
                    {
                        case Operation.Add:
                            {
                                Console.WriteLine("Pick figure:");
                                Console.WriteLine("\t 1: RoundShape");
                                Console.WriteLine("\t 2: Circle");
                                Console.WriteLine("\t 3: Ring");
                                Console.WriteLine("\t 4: Line");
                                Console.WriteLine("\t 5: Square");
                                Console.WriteLine("\t 6: Rectangle");
                                Console.WriteLine("\t 7: Triangle");
                                Console.WriteLine("\t 8: Back");
                                int input2 = Int32.Parse(Console.ReadLine());
                                f = (Figures)input2;
                                switch(f)
                                {
                                    case Figures.RoundShape:
                                    {
                                            Console.WriteLine("Insert center point and radius:");
                                            double x = Double.Parse(Console.ReadLine());
                                            double y = Double.Parse(Console.ReadLine());
                                            Point p = new Point(x, y);
                                            double r = Double.Parse(Console.ReadLine());
                                            if(r>0)
                                            box.AddShape(new RoundShape(p,r));
                                            else
                                                Console.WriteLine("Radius can't be negative");
                                    }
                                        break;

                                    case Figures.Circle:
                                        {
                                            Console.WriteLine("Insert center point and radius:");
                                            double x = Double.Parse(Console.ReadLine());
                                            double y = Double.Parse(Console.ReadLine());
                                            Point p = new Point(x, y);
                                            double r = Double.Parse(Console.ReadLine());
                                            if (r > 0)
                                                box.AddShape(new Circle(p, r));
                                            else
                                                Console.WriteLine("Radius can't be negative");
                                        }
                                        break;

                                    case Figures.Ring:
                                        {
                                            Console.WriteLine("Insert center point, inner and outher radius:");
                                            double x = Double.Parse(Console.ReadLine());
                                            double y = Double.Parse(Console.ReadLine());
                                            Point p = new Point(x, y);
                                            double ir = Double.Parse(Console.ReadLine());
                                            double or = Double.Parse(Console.ReadLine());
                                            if(ir>0 && or>0 && or>ir) 
                                            box.AddShape(new Ring(p,ir,or));
                                            else
                                                Console.WriteLine("Outer radius must be greater than inner and both must be greater than zero");
                                        }
                                        break;

                                    case Figures.Line:
                                        {
                                            Console.WriteLine("Insert starting point and ending point");
                                            double x = Double.Parse(Console.ReadLine());
                                            double y = Double.Parse(Console.ReadLine());
                                            Point p = new Point(x, y);
                                            double x1 = Double.Parse(Console.ReadLine());
                                            double y1 = Double.Parse(Console.ReadLine());
                                            Point p1 = new Point(x1, y1);
                                            box.AddShape(new Vector(p,p1));
                                        }
                                        break;

                                    case Figures.Square:
                                        {
                                            Console.WriteLine("Insert diagol's coordinates");
                                            double x = Double.Parse(Console.ReadLine());
                                            double y = Double.Parse(Console.ReadLine());
                                            Point p = new Point(x, y);
                                            double x1 = Double.Parse(Console.ReadLine());
                                            double y1 = Double.Parse(Console.ReadLine());
                                            Point p1 = new Point(x1, y1);
                                            box.AddShape(new Squre(p, p1));
                                           
                                        }
                                        break;
                                    case Figures.Rectangle:
                                        {
                                            Console.WriteLine("Insert center point and two sides");
                                            double x = Double.Parse(Console.ReadLine());
                                            double y = Double.Parse(Console.ReadLine());
                                            Point p = new Point(x, y);
                                            double x1 = Double.Parse(Console.ReadLine());
                                            double y1 = Double.Parse(Console.ReadLine());
                                            Point p1 = new Point(x1, y1);
                                            double x2 = Double.Parse(Console.ReadLine());
                                            double y2 = Double.Parse(Console.ReadLine());
                                            Point p2 = new Point(x2, y2);
                                            box.AddShape(new Parallelogram(p, p1, p2));

                                        }
                                        break;
                                    case Figures.Triangle:
                                        {
                                            Console.WriteLine("Insert center point and three sides");
                                            double x = Double.Parse(Console.ReadLine());
                                            double y = Double.Parse(Console.ReadLine());
                                            Point p = new Point(x, y);
                                            double x1 = Double.Parse(Console.ReadLine());
                                            double y1 = Double.Parse(Console.ReadLine());
                                            Point p1 = new Point(x1, y1);
                                            double x2 = Double.Parse(Console.ReadLine());
                                            double y2 = Double.Parse(Console.ReadLine());
                                            Point p2 = new Point(x2, y2);
                                            box.AddShape(new Triangle(p, p1, p2));
                                        }
                                        break;
                                    case Figures.Back:
                                        {

                                        }
                                        break;
                                }
                            }
                            break;
                        case Operation.Show:
                            {
                                box.ShowAll();
                            }
                            break;
                        case Operation.Delete:
                            {
                                box.RemoveAll();
                                Console.WriteLine("All figures now gone");
                            }
                            break;
                        case Operation.ChangeUser:
                            {
                                Console.WriteLine("Type new username");
                                string newname = Console.ReadLine();
                                user.SetName(newname);
                            }
                            break;

                    }
                }
            }

            catch (FormatException)
            {
                Console.WriteLine("Input is NULL");
            }

        }


    }
    enum Operation
    {
        Add=1,
        Show=2,
        Delete=3,
        ChangeUser = 4,
        Exit=5
    }
    enum Figures
    {
        RoundShape=1,
        Circle = 2,
        Ring = 3,
        Line =4,
        Square =5,
        Rectangle = 6,
        Triangle =7,
        Back=8
    }
}