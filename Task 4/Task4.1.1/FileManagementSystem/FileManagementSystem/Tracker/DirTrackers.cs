﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileManagementSystem.ChangesClass;
namespace FileManagementSystem.Tracker
{
    public class DirTrackers
    {
        private FileSystemWatcher watcher = new FileSystemWatcher();
        private List<Changes> changes = new List<Changes>();
        public bool Wathchflag { get; set; }
        public string PathToFolder { get; init; }
        public string PathToLog { get; init; }
        public string BackupPath { get; init; }
        public DirTrackers(string ptf, string ptb, bool flag)
        {
            this.Wathchflag = flag;
            this.PathToFolder = ptf;
            this.BackupPath = ptb;
            this.PathToLog = Path.Combine(BackupPath, "json.txt");
            this.watcher = new FileSystemWatcher(ptf);
            DirectoryInfo  mainfolderinfo = new DirectoryInfo(this.PathToFolder);
            DirectoryInfo  backuDirinfo = new DirectoryInfo(this.BackupPath);
            if(!Directory.Exists(BackupPath))
            {
                Directory.CreateDirectory(BackupPath);
            }
            if (!File.Exists(PathToLog))
            {
                using (FileStream fs = File.Create(PathToLog))
                {

                }
            }
            if (!Directory.Exists(Path.Combine(BackupPath,mainfolderinfo.Name)))
            {
                CreateBackup(true);
            }
            if (flag)
            {

                EventSubscribe();
            }

        }
        private void EventSubscribe()
        {
            watcher.NotifyFilter = NotifyFilters.Attributes
                                | NotifyFilters.CreationTime
                                | NotifyFilters.DirectoryName
                                | NotifyFilters.FileName
                                | NotifyFilters.LastAccess
                                | NotifyFilters.LastWrite
                                | NotifyFilters.Security
                                | NotifyFilters.Size;

            watcher.Changed += OnChanged;
            watcher.Created += OnCreated;
            watcher.Deleted += OnDeleted;
            watcher.Renamed += OnRenamed;
            watcher.Filter = "*.txt";
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;
        }
        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
            {
                return;
            }
            DateTime dt = DateTime.Now;
            changes.Clear();
            Changes change = new Changes("Changed", e.Name, "", e.FullPath, dt);
            changes.Add(change);
            JsonAppendLog(changes);
            CreateBackup(false);

        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            DateTime dt = DateTime.Now;
            changes.Clear();
            Changes change = new Changes("Created", e.Name, "", e.FullPath, dt);
            changes.Add(change);
            JsonAppendLog(changes);
            CreateBackup(false);
        }
        public void CreateBackup(bool IntitialBackupFlag)
        {
            if (IntitialBackupFlag==false)
            {
                var uniquename = Guid.NewGuid().ToString();
                DirectoryInfo directoryInfo = new DirectoryInfo(Path.Combine(BackupPath, uniquename));
                directoryInfo.Create();
                CopyDirectory(PathToFolder, directoryInfo.FullName, true);
            }
            else if(IntitialBackupFlag==true)
            {
                DirectoryInfo mainDirInfo = new DirectoryInfo(PathToFolder);
                DirectoryInfo directoryInfo = new DirectoryInfo(Path.Combine(BackupPath,mainDirInfo.Name));
                directoryInfo.Create();
                CopyDirectory(PathToFolder, directoryInfo.FullName, true);
            }
        }

        private void OnDeleted(object sender, FileSystemEventArgs e)
        {
            DateTime dt = DateTime.Now;
            Changes change = new Changes("Deleted", e.Name, " ", e.FullPath, dt);
            changes.Add(change);
            JsonAppendLog(changes);
            CreateBackup(false);
        }
        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            if (e.Name == e.OldName)
            {
                return;
            }
            else
            {
                DateTime dt = DateTime.Now;
                changes.Clear();
                Changes change = new Changes("Renamed", e.Name, e.OldName, e.FullPath, dt);
                changes.Add(change);
                JsonAppendLog(changes);
                CreateBackup(false);
            }
        }
        public void JsonAppendLog(List<Changes> c)
        {
            List<Changes> prevlist = new List<Changes>();
            using (StreamReader reader = new StreamReader(PathToLog))
            using (JsonReader jsreader = new JsonTextReader(reader))
            {
                string temp = reader.ReadToEnd();
                if (!string.IsNullOrEmpty(temp))
                {
                    prevlist = JsonConvert.DeserializeObject<List<Changes>>(temp);
                }
                else
                {

                }
            }
            prevlist.AddRange(c);
            JsonSerializer jsonSerializer = new JsonSerializer();
            File.WriteAllText(PathToLog, string.Empty);
            using (StreamWriter sw = new StreamWriter(PathToLog))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                jsonSerializer.Serialize(writer, prevlist);
            }
        }
        public void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
        {
            var dir = new DirectoryInfo(sourceDir);
            var destdir = new DirectoryInfo(destinationDir);

            if (!dir.Exists)
                throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!destdir.Exists)
            {
                Directory.CreateDirectory(destinationDir);
            }

            foreach (FileInfo file in dir.GetFiles())
            {
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
            }

            if (recursive)
            {
                foreach (DirectoryInfo subDir in dirs)
                {
                    string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                    CopyDirectory(subDir.FullName, newDestinationDir, true);
                }
            }
        }
        public void Backup()
        {
            DirectoryInfo di = new DirectoryInfo(BackupPath);
            List<DateTime> list = new List<DateTime>();
            DirectoryInfo[] directoryInfos = di.GetDirectories();
            if (directoryInfos.Count() > 0)
            {
                foreach (DirectoryInfo directoryInfo in directoryInfos)
                {
                    list.Add(Directory.GetCreationTime(directoryInfo.FullName));
                }
                var sorted = directoryInfos.OrderBy(x => Directory.GetCreationTime(x.FullName));
                Console.WriteLine("Avalible backups");
                foreach (DirectoryInfo dir in sorted)
                {
                    Console.WriteLine(Directory.GetCreationTime(dir.FullName));
                }
                Console.WriteLine("Input DateTime:");
                DateTime dt = DateTime.Parse(Console.ReadLine());
                if (dt < sorted.Select(x => Directory.GetCreationTime(x.FullName)).FirstOrDefault() || dt > sorted.Select(x => Directory.GetCreationTime(x.FullName)).LastOrDefault())
                {
                    Console.WriteLine("No backups for this date");
                }
                else
                {
                    var res = sorted.Select(x => x).Where(x => DateTime.Compare(dt, Directory.GetCreationTime(x.FullName)) >= 0).LastOrDefault();
                    Directory.Delete(PathToFolder, true);
                    CopyDirectory(res.FullName, PathToFolder, true);
                }
            }
            else
            {
                Console.WriteLine("No avalible backups");
            }
        }
    }
}
