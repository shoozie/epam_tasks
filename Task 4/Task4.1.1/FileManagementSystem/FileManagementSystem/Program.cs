﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using FileManagementSystem.Tracker;
namespace FilesTask 
{
    public class Program
    {
        static void Main()
        {
            var path_toDir = @"D:\MyFiles"; //input your dir
            var path_toLog = @"D:\TestLog\json.txt";//input your file
            var path_toBackup = @"D:\TestLog";//input your dir
            DirectoryInfo directoryInfo = new DirectoryInfo(path_toDir);
            if (!directoryInfo.Exists)
            {
                throw new DirectoryNotFoundException("Directory not found");
            }
            while (true)
            {
                Console.WriteLine("Pick mode:");
                Console.WriteLine("1:Spy mode");
                Console.WriteLine("2:Backup mode");
                int input = Int32.Parse(Console.ReadLine());
                switch (input)
                {
                    case 1:
                        {
                            DirTrackers dt = new DirTrackers(path_toDir, path_toBackup, true);
                            Console.WriteLine($"Paranoid Android is watching for {path_toDir}");
                            Console.WriteLine("Press any key to exit");
                            Console.ReadLine();
                        }
                        break;
                    case 2:
                        {
                            DirTrackers dt = new DirTrackers(path_toDir, path_toBackup, false);
                            dt.Backup();
                        }
                        break;
                }
            }
        }
    }
}