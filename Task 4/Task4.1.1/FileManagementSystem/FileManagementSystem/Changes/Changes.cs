﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementSystem.ChangesClass
{
    public class Changes
    {
        public string OldName { get; set; }
        public string Name { get; set; }
        public string ChangeType { get; set; }
        public string FullPath { get; set; }
        public DateTime changetime;
        public Changes(string changetype, string name, string oldname, string fullpath, DateTime dateTime)
        {
            this.OldName = oldname;
            this.Name = name;
            this.ChangeType = changetype;
            this.FullPath = fullpath;
            this.changetime = dateTime;
        }
    }
}
